package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        float averageLength = 0;
        System.out.println("Введите количество строк: ");
        int kolichestvoStrok = new Scanner(System.in).nextInt();
        String[] arr = new String[kolichestvoStrok];
        for (int i = 0; i < kolichestvoStrok; i++) {
            System.out.println("Введите " + (i + 1) + "ю строку");
            arr[i] = new Scanner(System.in).nextLine();
            averageLength += arr[i].length() / kolichestvoStrok;
        }
        for (int j = 0; j < kolichestvoStrok; j++) {
            if (averageLength > arr[j].length()) {
                System.out.println("Строка №" + (j + 1) + ": " + arr[j]);
            }
        }
    }
}